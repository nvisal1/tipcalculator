package com.example.anthonyvisalli.tipcalculator_visalli;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
//import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;

public class TipCalculatorActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    EditText enterAmount;
    Button convertBtn;
    private TextView spinnerResult;
    TextView errorMessage;
    TextView totalPrompt;
    TextView tipPrompt;
    TextView totalResult;
    TextView tipResult;

    double amount;
    double percent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tip_calculator);

        enterAmount = findViewById(R.id.enterValue);
        convertBtn = findViewById(R.id.button);
        convertBtn.setOnClickListener(this);
        errorMessage = findViewById(R.id.errorMessage);
        totalPrompt = findViewById(R.id.total);
        totalResult = findViewById(R.id.totalResult);
        tipPrompt = findViewById(R.id.tip);
        tipResult = findViewById(R.id.tipResult);

        setUpSpinner();

    }

    private void setUpSpinner() {
        Spinner spinner = findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.percent_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        //spinnerResult = findViewById(R.id.spinnerResult);
        spinner.setOnItemSelectedListener(this);
    }

    //@Overrride
    public void onClick (View view)
    {
        switch(view.getId())
        {
            case R.id.button:
                calculate();
                break;
        }
    }

    private void calculate() {

        String check = enterAmount.getText().toString();

        if(check.matches("")) {
            tipResult.setVisibility(View.GONE);
            totalResult.setVisibility(View.GONE);
            tipPrompt.setVisibility(View.GONE);
            totalPrompt.setVisibility(View.GONE);
            errorMessage.setVisibility(View.VISIBLE);
        } else {
            errorMessage.setVisibility(View.GONE);
            amount = Double.parseDouble(enterAmount.getText().toString());
            double tip = amount * percent;
            double total = amount + tip;

            displayResult(tip, total);
        }

    }

    private void displayResult(double tip, double total) {
        DecimalFormat f = new DecimalFormat("##.00");
        String total_string = f.format(total);
        String tip_string = f.format(tip);
        totalResult.setText(total_string);
        tipResult.setText(tip_string);

        tipResult.setVisibility(View.VISIBLE);
        totalResult.setVisibility(View.VISIBLE);
        tipPrompt.setVisibility(View.VISIBLE);
        totalPrompt.setVisibility(View.VISIBLE);
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        percent = Double.parseDouble(adapterView.getSelectedItem().toString());

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

